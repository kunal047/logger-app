const fs        = require('fs');
const md5       = require('md5');
const chokidar  = require("chokidar");

const socketIOClient  = require("socket.io-client");
const sailsIOClient   = require("sails.io.js");

const io = sailsIOClient(socketIOClient);
io.sails.url = "http://localhost:1337";

const loggerFile = 'PATH_TO_LOGGER.TXT';

module.exports = {

    index : (req, res, next) => {
        return res.view('pages/index');
    },

    save: (req, res, next) => {

        var name = req.param('fullname');
        if (name && name.length > 0) {
            res.send("ok");
        } else {
            res.send("not ok");
        }
    },
    logs: (req, res, next) => {
        
        var md5Previous = null;
        if (!req.isSocket) {
            sails.log("Only a client socket can subscribe to Logs.  But you look like an HTTP request to me");
        }
        var socket = sails.sockets.parseSocket(req);
        sails.sockets.join(socket, 'logs');
        
        chokidar.watch(loggerFile).on('change', (path, stats) => {
            data = fs.readFileSync(loggerFile);
            // checking if content of file changed using md5
            const md5Current = md5(data);
            if (md5Current !== md5Previous) {
                md5Previous = md5Current;
                fs.readFile(loggerFile, (err, data) => {
                    if (err) throw err;
                    theFile = data.toString().split("\n");
                    last10Lines = theFile.slice(-11, -1);
                    sails.sockets.broadcast('logs', {
                        file: last10Lines
                    });
                });   
            }
        });
        return res.send("success");
    },

    log: (req, res, next) => {
        return res.view('pages/log');
    }

};